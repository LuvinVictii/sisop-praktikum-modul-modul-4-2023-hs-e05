#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <libgen.h>

#ifndef S_IFREG
#define S_IFREG 0100000 // Regular file bit mask (octal representation)
#endif

#define MAX_PATH 4096

#define MAX_PASSWORD_LENGTH 100

char PASSWORD[MAX_PASSWORD_LENGTH] = "mypass";


char SOURCE_DIR[MAX_PATH];
char MOUNT_POINT[MAX_PATH];

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const unsigned char* data, size_t inputLen) {
    printf("Former Name: %s\n", data);
    size_t output_length = 4 * ((inputLen + 2) / 3);
    char* hasilEncode = (char*)malloc(output_length + 1); 

    size_t i, j;
    for (i = 0, j = 0; i < inputLen; i += 3, j += 4) {
        unsigned char octet_a = i < inputLen ? data[i] : 0;
        unsigned char octet_b = i + 1 < inputLen ? data[i + 1] : 0;
        unsigned char octet_c = i + 2 < inputLen ? data[i + 2] : 0;

        hasilEncode[j] = base64_table[octet_a >> 2];
        hasilEncode[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        hasilEncode[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        hasilEncode[j + 3] = base64_table[octet_c & 63];
    }

    if (inputLen % 3 == 1) {
        hasilEncode[output_length - 2] = '=';
        hasilEncode[output_length - 1] = '=';
    } else if (inputLen % 3 == 2) {
        hasilEncode[output_length - 1] = '=';
    }

    hasilEncode[output_length] = '\0';
    printf("Encoded Name: %s\n", hasilEncode);
    return hasilEncode;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[MAX_PATH*2];

    sprintf(fpath,"%s%s",SOURCE_DIR,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    (void) offset;
    (void) fi;
    dp = opendir(fpath);
    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char filter_name[MAX_PATH];
        memset(filter_name, 0, sizeof(filter_name));

        strcpy(filter_name, de->d_name);

        // kondisi LUTH
        if (filter_name[0] == 'L' || filter_name[0] == 'l' ||
            filter_name[0] == 'U' || filter_name[0] == 'u' ||
            filter_name[0] == 'T' || filter_name[0] == 't' ||
            filter_name[0] == 'H' || filter_name[0] == 'h') {
            
            char* encoded = base64_encode((unsigned char*)filter_name, strlen(filter_name));
            snprintf(filter_name, sizeof(filter_name), "%s", encoded);
            printf("Convert Name: %s\n", filter_name);
            free(encoded);
        }

        // kondisi < 5 ch
        if (strlen(filter_name) < 5) {
            printf("Less than 5: %s\n", filter_name);

            char new_conv[MAX_PATH];
            memset(new_conv, 0, sizeof(new_conv));
            for (size_t i = 0; i < strlen(filter_name); i++) {
                char binary[10];
                binary[8] = ' ';
                binary[9] = '\0';

                unsigned char ch = filter_name[i];
                for (int j = 7; j >= 0; j--) {
                    binary[j] = (ch & 1) + '0';
                    ch >>= 1;
                }

                strcat(new_conv, binary);
            }
            memset(filter_name, 0, sizeof(filter_name));
            strcpy(filter_name, new_conv);
            printf("After convert 5: %s\n\n", filter_name);
        }

        if (S_ISREG(st.st_mode)) {

            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = tolower(filter_name[i]);
            }

            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);

            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);

            rename(old_dir_path, new_dir_path);
        }
        else{

            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = toupper(filter_name[i]);
            }

            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);

            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);

            rename(old_dir_path, new_dir_path);
        }

        if (S_ISDIR(st.st_mode)) {

            char new_path[MAX_PATH*2];
            snprintf(new_path, sizeof(new_path), "%s/%s", path, filter_name);
            xmp_readdir(new_path, buf, filler, offset, fi);
        }

        filler(buf, filter_name, &st, 0);
    }

    closedir(dp);
    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[MAX_PATH*2];
    if(strcmp(path,"/") == 0){
        path=SOURCE_DIR;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",SOURCE_DIR,path);

    int res = 0;
    int fileDIr = 0 ;

    (void) fi;

    fileDIr = open(fpath, O_RDONLY);

    if (fileDIr == -1) return -errno;

    res = pread(fileDIr, buf, size, offset);

    if (res == -1) res = -errno;

    close(fileDIr);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode){
    int res;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    int fileDIr;
    int res;
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    (void)fi;
    fileDIr = open(fpath, O_WRONLY);
    if (fileDIr == -1)
        return -errno;

    res = pwrite(fileDIr, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fileDIr);
    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir, 
    .write = xmp_write, 
};


int main(int  argc, char *argv[]) {
  mkdir("mod", 0777);
  pid_t child_id;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    pid_t child_id1;

    child_id1 = fork();

    if (child_id1 < 0) {
      exit(EXIT_FAILURE);
    }

    if (child_id1 == 0) {
      char *argv[] = {"wget", "-O", "inifolderetc.zip", "drive.google.com/u/3/uc?id=1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT&export=download&confirm=yes", NULL};
      execv("/bin/wget", argv);
    }
    else{
      wait(NULL);
      char *argv[] = {"unzip", "inifolderetc.zip", NULL};
      execv("/usr/bin/unzip", argv);
    }

    
  } else {

    wait(NULL);

    umask(0);

    char cwd[1000];
    
    getcwd(cwd, sizeof(cwd));
    snprintf(SOURCE_DIR, sizeof(SOURCE_DIR), "%s/inifolderetc/sisop", cwd);
    snprintf(MOUNT_POINT, sizeof(MOUNT_POINT), "%s/mod", cwd);

    return fuse_main(argc, argv, &xmp_oper, NULL);
  }
}
