# sisop-praktikum-modul-modul-4-2023-hs-e05

| Nama                    | NRP        |
| -----------------------| -----------|
| Muhammad Daffa Harits   | 5025211005 |
| Shafa Nabilah Hanin     | 5025211222 |
| Anggara Saputra         | 5025211241 |

## Soal 3

### Cara pengerjaan:

A. Fungsi `base64_encode` adalah sebuah fungsi yang digunakan untuk melakukan encoding data menggunakan metode Base64. Berikut adalah penjelasan mengenai langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Pertama, fungsi menerima dua parameter:
   - `const unsigned char* data`: Pointer ke array data yang akan diencode.
   - `size_t inputLen`: Ukuran data yang akan diencode.

2. Kemudian, fungsi menghitung panjang output dari encoding Base64 dengan menggunakan rumus: `output_length = 4 * ((inputLen + 2) / 3)`. Hal ini menghasilkan ukuran array `hasilEncode` yang akan digunakan untuk menyimpan hasil encoding.

3. Fungsi melakukan alokasi memori untuk `hasilEncode` menggunakan `malloc` dengan ukuran `output_length + 1` untuk menyertakan karakter null-terminator.

4. Fungsi melakukan iterasi untuk mengambil setiap tiga karakter dari data input dan mengkonversinya menjadi empat karakter hasil encoding Base64. Iterasi ini dilakukan menggunakan variabel `i` untuk melacak indeks pada data input dan variabel `j` untuk melacak indeks pada `hasilEncode`.

5. Dalam setiap iterasi, tiga karakter diambil dari data input dan diubah menjadi empat karakter hasil encoding Base64 menggunakan tabel `base64_table`. Proses ini melibatkan pemindahan bit dan operasi logika bitwise.

6. Setelah iterasi selesai, fungsi memeriksa jika panjang data input tidak habis dibagi tiga. Jika sisa satu karakter, dua karakter padding '=' ditambahkan di akhir hasil encoding. Jika sisa dua karakter, satu karakter padding '=' ditambahkan di akhir hasil encoding.

7. Karakter null-terminator ('\0') ditambahkan di akhir hasil encoding untuk menandakan akhir dari string.

8. Fungsi mencetak nama asli (`data`) dan hasil encoding (`hasilEncode`) menggunakan `printf`.

9. Fungsi mengembalikan pointer ke `hasilEncode`.

Dengan menggunakan fungsi `base64_encode`, data dapat diencode menggunakan metode Base64 sehingga dapat digunakan dalam konteks yang membutuhkan representasi teks yang aman dan portabel.

B. Fungsi `xmp_getattr` digunakan dalam FUSE untuk mendapatkan atribut dari sebuah file atau direktori yang diidentifikasi oleh `path`. Berikut adalah penjelasan mengenai langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Fungsi menerima dua parameter:
   - `const char *path`: Path relatif dari file atau direktori yang atributnya ingin diambil.
   - `struct stat *stbuf`: Pointer ke struktur `stat` yang akan diisi dengan atribut dari file atau direktori.

2. Fungsi mendeklarasikan variabel `res` untuk menyimpan hasil dari pemanggilan fungsi `lstat`.

3. Fungsi menyusun full path dari file atau direktori dengan menggabungkan `SOURCE_DIR` (path direktori sumber) dengan `path` yang diberikan. Ini dilakukan dengan menggunakan `sprintf` untuk memasukkan nilai `SOURCE_DIR` dan `path` ke dalam string `fpath`.

4. Fungsi memanggil fungsi `lstat` dengan `fpath` untuk mendapatkan atribut dari file atau direktori yang sesuai dengan `path`. `lstat` adalah fungsi sistem yang digunakan untuk mendapatkan informasi atribut dari sebuah file atau direktori.

5. Fungsi memeriksa nilai kembalian dari `lstat`. Jika nilai kembalian adalah -1, maka terjadi error dalam memanggil `lstat`, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

6. Jika tidak ada error, fungsi akan mengembalikan nilai 0 untuk menunjukkan bahwa operasi `getattr` berhasil dilakukan.

Dengan menggunakan fungsi `xmp_getattr`, FUSE dapat mendapatkan atribut (seperti ukuran, waktu modifikasi, dan hak akses) dari file atau direktori yang diperlukan saat melakukan operasi filesystem pada FUSE.

C. Fungsi `xmp_readdir` digunakan dalam FUSE untuk membaca isi dari sebuah direktori yang diidentifikasi oleh `path` dan mengisi struktur `buf` dengan informasi file/direktori menggunakan fungsi `filler`. Berikut adalah penjelasan langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Fungsi mendeklarasikan variabel-variabel yang akan digunakan:
   - `DIR *dp`: Pointer ke struktur `DIR` yang digunakan untuk mewakili direktori yang akan dibuka.
   - `struct dirent *de`: Pointer ke struktur `dirent` yang digunakan untuk menyimpan informasi dari setiap entri di direktori.
   - `char fpath[MAX_PATH]`: String yang digunakan untuk menyimpan full path dari direktori yang akan dibaca.
   - `char filter_name[MAX_PATH]`: String yang digunakan untuk menyimpan nama file/direktori yang akan dimodifikasi.

2. Fungsi menggunakan `snprintf` untuk menyusun full path dari direktori yang akan dibaca dengan menggabungkan `SOURCE_DIR` dan `path` yang diberikan.

3. Fungsi menggunakan `opendir` untuk membuka direktori yang sesuai dengan `fpath`. Jika `opendir` mengembalikan nilai `NULL`, artinya terjadi error, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

4. Fungsi melakukan loop menggunakan `readdir` untuk membaca setiap entri (file/direktori) di dalam direktori yang dibuka. Loop ini akan berjalan selama masih ada entri yang belum dibaca.

5. Setiap entri yang dibaca dicek jika nama entri adalah `.` (direktori itu sendiri) atau `..` (direktori induk), maka entri tersebut diabaikan dan loop berlanjut ke entri berikutnya.

6. Untuk setiap entri, fungsi membuat sebuah struktur `stat` (`st`) dan mengisi informasi yang relevan seperti inode (`st_ino`) dan mode (`st_mode`) dengan menggunakan nilai yang diberikan oleh `dirent`. Nilai mode (`st_mode`) diperoleh dengan melakukan shift (`<<`) pada tipe entri (`d_type`) dan menggabungkannya dengan konstanta `12`.

7. Fungsi melakukan filter terhadap nama entri dengan memeriksa kondisi sebagai berikut:
   - Jika huruf pertama dari nama entri adalah L, U, T, atau H (baik lowercase atau uppercase), maka nama entri akan diencode menggunakan fungsi `base64_encode` yang mengembalikan string yang dienkripsi. String hasil encode tersebut kemudian digunakan sebagai nama entri yang baru.
   - Jika panjang nama entri kurang dari 5 karakter, maka nama entri akan diubah menjadi representasi biner dari setiap karakter dalam nama tersebut.

8. Jika entri adalah file (`S_ISREG(st.st_mode)`), nama entri diubah menjadi lowercase menggunakan loop `for` dan kemudian direktori tersebut diubah namanya menggunakan fungsi `rename`. Jika entri adalah direktori (`S_ISDIR(st.st_mode)`), nama entri diubah menjadi uppercase menggunakan loop `for` dan kemudian dilakukan pemanggilan rekursif ke fungsi `xmp_readdir` dengan path yang baru.

9. Fungsi memanggil fungsi `filler`

 untuk mengisi struktur `buf` dengan informasi entri yang baru (termasuk nama dan atribut).

10. Setelah selesai membaca semua entri di direktori, direktori ditutup menggunakan `closedir`.

11. Fungsi mengembalikan nilai 0 untuk menunjukkan bahwa operasi `readdir` telah selesai dengan sukses.

Dengan menggunakan fungsi `xmp_readdir`, FUSE dapat membaca isi dari direktori dan melakukan modifikasi pada nama file/direktori sesuai dengan kondisi yang ditentukan dalam fungsi tersebut.

D. Fungsi `xmp_read` digunakan dalam FUSE untuk membaca isi dari sebuah file yang diidentifikasi oleh `path` dan mengembalikan data yang dibaca melalui parameter `buf`. Berikut adalah penjelasan langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Fungsi mendeklarasikan variabel-variabel yang akan digunakan:
   - `char fpath[MAX_PATH*2]`: String yang digunakan untuk menyimpan full path dari file yang akan dibaca.

2. Fungsi melakukan pengecekan jika `path` adalah root direktori ("/"). Jika iya, maka `path` diganti dengan `SOURCE_DIR` yang merupakan direktori sumber yang telah ditentukan sebelumnya. Selanjutnya, `fpath` disusun dengan menggunakan `sprintf` dengan menggabungkan `SOURCE_DIR` dan `path`.

3. Jika `path` bukan root direktori, maka `fpath` disusun dengan menggunakan `sprintf` dengan menggabungkan `SOURCE_DIR` dan `path`.

4. Fungsi mendeklarasikan variabel `res` dengan nilai 0 dan `fileDIr` dengan nilai 0. Variabel `res` akan digunakan untuk menyimpan hasil operasi pembacaan, dan `fileDIr` akan digunakan untuk menyimpan file descriptor dari file yang akan dibaca.

5. Fungsi menggunakan `open` untuk membuka file yang sesuai dengan `fpath` dalam mode pembacaan (`O_RDONLY`). Jika `open` mengembalikan nilai `-1`, artinya terjadi error, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

6. Fungsi menggunakan `pread` untuk membaca isi file yang terbuka (`fileDIr`) ke dalam buffer (`buf`). `size` menentukan jumlah byte yang akan dibaca, dan `offset` menentukan posisi dari mana pembacaan akan dimulai. Jika `pread` mengembalikan nilai `-1`, artinya terjadi error, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

7. Setelah selesai membaca file, file ditutup menggunakan `close(fileDIr)`.

8. Fungsi mengembalikan nilai `res` yang berisi jumlah byte yang berhasil dibaca. Jika ada error dalam proses pembacaan, maka fungsi mengembalikan nilai negatif dari `errno`.

Dengan menggunakan fungsi `xmp_read`, FUSE dapat membaca isi dari file yang terletak di direktori yang telah dimodifikasi.

E. Fungsi `xmp_mkdir` digunakan dalam FUSE untuk membuat direktori baru dengan path yang ditentukan oleh `path` dan mode akses yang ditentukan oleh `mode`. Berikut adalah penjelasan langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Fungsi mendeklarasikan variabel `res` yang akan digunakan untuk menyimpan hasil operasi pembuatan direktori.

2. Fungsi menggunakan `snprintf` untuk menyusun full path dari direktori yang akan dibuat. Full path tersebut disimpan dalam variabel `fpath`, yang dihasilkan dengan menggabungkan `SOURCE_DIR` (direktori sumber) dan `path`.

3. Fungsi menggunakan `mkdir` untuk membuat direktori baru dengan path yang ditentukan oleh `fpath` dan mode akses yang ditentukan oleh `mode`. Jika `mkdir` mengembalikan nilai `-1`, artinya terjadi error, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

4. Jika pembuatan direktori berhasil, fungsi akan mengembalikan nilai 0 yang menunjukkan operasi berhasil dilakukan.

5. Jika terjadi error dalam proses pembuatan direktori, fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

Dengan menggunakan fungsi `xmp_mkdir`, FUSE dapat membuat direktori baru di dalam direktori yang telah dimodifikasi sesuai dengan path yang diberikan.

F. Fungsi `xmp_write` digunakan dalam FUSE untuk menulis data ke dalam sebuah file dengan path yang ditentukan oleh `path`. Berikut adalah penjelasan langkah-langkah yang dilakukan dalam fungsi tersebut:

1. Fungsi mendeklarasikan variabel `fileDIr` dan `res`. `fileDIr` digunakan untuk menyimpan file descriptor dari file yang akan ditulis, sedangkan `res` digunakan untuk menyimpan hasil operasi penulisan.

2. Fungsi menggunakan `snprintf` untuk menyusun full path dari file yang akan ditulis. Full path tersebut disimpan dalam variabel `fpath`, yang dihasilkan dengan menggabungkan `SOURCE_DIR` (direktori sumber) dan `path`.

3. Fungsi menggunakan `open` dengan mode `O_WRONLY` untuk membuka file dengan path yang ditentukan oleh `fpath`. Jika `open` mengembalikan nilai `-1`, artinya terjadi error dalam pembukaan file, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

4. Jika pembukaan file berhasil, fungsi akan melanjutkan untuk menulis data ke dalam file.

5. Fungsi menggunakan `pwrite` untuk menulis data dari buffer `buf` ke dalam file dengan file descriptor `fileDIr`. Ukuran data yang akan ditulis ditentukan oleh `size`, dan posisi awal penulisan ditentukan oleh `offset`. Jika `pwrite` mengembalikan nilai `-1`, artinya terjadi error dalam penulisan, dan fungsi akan mengembalikan nilai negatif dari `errno` yang menunjukkan jenis error yang terjadi.

6. Jika penulisan berhasil, `res` akan berisi jumlah byte yang berhasil ditulis ke dalam file.

7. Fungsi akan menutup file dengan menggunakan `close(fileDIr)`.

8. Fungsi mengembalikan nilai `res`, yang merupakan hasil operasi penulisan. Jika penulisan berhasil, nilai positif akan menunjukkan jumlah byte yang berhasil ditulis. Jika terjadi error, nilai negatif akan menunjukkan jenis error yang terjadi.

Dengan menggunakan fungsi `xmp_write`, FUSE dapat menulis data ke dalam file di dalam direktori yang telah dimodifikasi sesuai dengan path yang diberikan.

G. Pada potongan kode yang diberikan, terdapat implementasi dari program FUSE yang menggunakan fungsi-fungsi yang telah dijelaskan sebelumnya. Berikut adalah penjelasan langkah-langkah yang dilakukan dalam program tersebut:

1. Deklarasi struct `fuse_operations` bernama `xmp_oper` yang menyimpan fungsi-fungsi operasi yang akan digunakan dalam FUSE.

2. Di dalam fungsi `main`, terdapat proses pembuatan direktori `mod` dengan menggunakan `mkdir("mod", 0777)`. Direktori ini akan digunakan sebagai mount point untuk FUSE.

3. Dilakukan fork pertama dengan `child_id = fork()` untuk membuat proses anak.

4. Pada proses anak pertama, dilakukan fork kedua dengan `child_id1 = fork()` untuk membuat proses anak kedua.

5. Pada proses anak kedua (`child_id1 == 0`), dilakukan eksekusi perintah `wget` menggunakan `execv` untuk mendownload file `inifolderetc.zip` dari URL yang diberikan.

6. Setelah proses anak kedua selesai (`wait(NULL)`), dilakukan ekstraksi file `inifolderetc.zip` menggunakan perintah `unzip` dengan `execv`.

7. Kembali ke proses anak pertama setelah ekstraksi selesai, dan setelah proses anak pertama selesai (`wait(NULL)`), dilakukan pemanggilan `umask(0)` untuk mengatur umask menjadi 0.

8. Dilakukan pemanggilan `getcwd` untuk mendapatkan current working directory (cwd) dan menyimpannya dalam variabel `cwd`.

9. Nilai `cwd` digunakan untuk menyusun path lengkap dari direktori sumber (`SOURCE_DIR`) dan mount point (`MOUNT_POINT`) dengan menggunakan `snprintf`.

10. Terakhir, dilakukan pemanggilan `fuse_main` dengan argumen `argc`, `argv`, `&xmp_oper`, dan `NULL` untuk menjalankan program FUSE dengan operasi-operasi yang telah ditentukan sebelumnya.

Dengan demikian, program ini akan menjalankan FUSE yang akan menghubungkan direktori sumber (`SOURCE_DIR`) dengan direktori mount (`MOUNT_POINT`) dan mengimplementasikan fungsi-fungsi seperti `getattr`, `readdir`, `read`, `mkdir`, dan `write` yang telah didefinisikan sebelumnya.

### Kendala
Kendala yang dialami saat menjalankan program adalah program berhenti berjalan di tengah running

![error](img/error.png)
